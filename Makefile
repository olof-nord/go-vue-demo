# For more info about the syntax, see https://makefiletutorial.com/

CURRENT_DIR = $(shell pwd)
export WEB_DIR = $(CURRENT_DIR)/frontend/dist

build: build-frontend build-backend

build-frontend:
	@cd frontend && npm run build

build-backend:
	@cd backend && go build -v .

start:
	$(CURRENT_DIR)/backend/go-vue-backend-demo
