package main

import (
    "gitlab.com/olof-nord/go-vue-backend-demo/webserver"
)

func main() {
    webserver.Run();
}
